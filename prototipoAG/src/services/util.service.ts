import { Injectable } from '@angular/core';
import { Config } from '../environments/config';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Subject } from 'rxjs';
import { DataSubjectModel } from '../models/dataSubject.model';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  private _ambiente: string = Config.AMBIENTES.prd;


  public FuncaoOrigem = new Subject<any>();
  public FuncaoChamada = this.FuncaoOrigem.asObservable();

  public specificSubjects = {
    pesquisa: new Subject<any>(),
    grid: new Subject<any>(),
    comandBar: new Subject<any>(),
    modalEditar: new Subject<any>(),
    modalIncluir: new Subject<any>(),
    rotaEditar: new Subject<any>(),
    rotaIncluir: new Subject<any>(),
    gridfiltro: new Subject<any>()
  };
  private __gridSelectedRows: any[] = [];
  constructor(
    public messageService: MessageService,
    public confirmationService: ConfirmationService
  ) { }

  /**
   * Dispara mensagens de alerta
   * @param texto
   * @param titulo
   * @param tipo
   */
  public alerta(texto: string, titulo: string, tipo?: string) {
    if (tipo === undefined) {
      tipo = Config.TIPOMENSAGEM.success;
    }
    this.messageService.add({ severity: tipo, summary: titulo, detail: texto });
  }

  /**
   * retorna a url dos dados com base no ambiente
   */
  get urlObjeto() {
    switch (this._ambiente) {
      case Config.AMBIENTES.prd:
        return Config.CAMINHODADOS.url_prd;
      case Config.AMBIENTES.hmg:
        return Config.CAMINHODADOS.url_hmg;
      case Config.AMBIENTES.dsv:
        return Config.CAMINHODADOS.url_prd;
    }

  }

  /**
 * retorna a url dos dados com base no ambiente
 */
  get urlColunas() {
    switch (this._ambiente) {
      case Config.AMBIENTES.prd:
        return Config.CAMINHO_COLUMNS_DEF.url_prd;
      case Config.AMBIENTES.hmg:
        return Config.CAMINHO_COLUMNS_DEF.url_hmg;
      case Config.AMBIENTES.dsv:
        return Config.CAMINHO_COLUMNS_DEF.url_prd;
    }

  }


  public set gridSelectedRows(gridRow) {
    // Precisamos Mudar.
    this.__gridSelectedRows = gridRow;
  }

  public get gridSelectedRows(): Array<any> {
    // Precisamos Mudar.
    return this.__gridSelectedRows;
  }

  /**
   *Esta função serve para propagar Subjects
   * @param valor
   * @param id
   */
  ChamarFuncao(valor: any, idComp: string, idSub: string, EscopoPesquisa: string) {
    const comando: DataSubjectModel = { dados: valor, idSubject: idSub, idComponent: idComp, escopoPesquisa: EscopoPesquisa };
    this.FuncaoOrigem.next(comando);

    switch (idSub) {
      case Config.IDSUBJECT.comandBar:
        this.specificSubjects.comandBar.next(comando);
        break;
      case Config.IDSUBJECT.grid:
        this.specificSubjects.grid.next(comando);
        break;
      case Config.IDSUBJECT.modalEditar:
        this.specificSubjects.modalEditar.next(comando);
        break;
      case Config.IDSUBJECT.modalIncluir:
        this.specificSubjects.modalIncluir.next(comando);
        break;
      case Config.IDSUBJECT.pesquisa:
        this.specificSubjects.pesquisa.next(comando);
        break;
      case Config.IDSUBJECT.rotaEditar:
        this.specificSubjects.rotaEditar.next(comando);
        break;
      case Config.IDSUBJECT.rotaIncluir:
        this.specificSubjects.rotaIncluir.next(comando);
        break;
      case Config.IDSUBJECT.gridfiltro:
        this.specificSubjects.gridfiltro.next(comando);
        break;  
    }
  }
}

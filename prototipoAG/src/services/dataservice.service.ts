import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { DataSubjectModel } from '../models/dataSubject.model';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Config } from '../environments/config';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EditarModel } from '../models/editar.model';


@Injectable()

export class DataService {
  private _dados: any;

  /* httpResponse: HttpResponse; */
  constructor(public UtilSrv: UtilService
    , public httpClient: HttpClient) {
    this.UtilSrv.FuncaoChamada
      .subscribe((data: DataSubjectModel) => {
        this.tratarAcao(data);
      });
  }

  /**
   * Getter dados
   *
   */
  public get dados(): any {
    return this._dados;
  }

  public filtrarGridDescricao(PesquisaId: string,descricao: string){
    return this.httpClient.get(Config.URLS.BASE + Config.URLS.URL_DADOS_DEV[0].DADOS_GRID_FILTRO_DESCRICAO + descricao)
    .pipe(map(data => {
      console.log('url',Config.URLS.BASE + Config.URLS.URL_DADOS_DEV[0].DADOS_GRID_FILTRO_DESCRICAO + descricao)
       console.log('DATA FILTRADA', data);
      this._dados = data;
      this.UtilSrv.ChamarFuncao('', Config.IDSUBJECT.grid, Config.IDSUBJECT.dadoslidos, PesquisaId);
      //return data;
    }));
  }

  public fechDataBasicGrid(PesquisaId: string) {
    return this.httpClient.get(Config.URLS.LOCAL + Config.URLS.URL_DADOS_DEV[0].DADOS_GRID_BASICA)
      .pipe(map(data => {
        // console.log('DATA NO GET FER FAFOS', data);
        this._dados = data;
        this.UtilSrv.ChamarFuncao('', Config.IDSUBJECT.grid, Config.IDSUBJECT.dadoslidos, PesquisaId);
      }));
  }

  public salvarDados(ItensParaSalvar) {
    console.log('Dentro da linha 41, na função de salvar', ItensParaSalvar);
    const JsonObj = JSON.stringify(ItensParaSalvar);
    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json'
        })
    };
    return this.httpClient.post(Config.URLS.BASE + Config.URLS.URL_DADOS_DEV[0].DADOS_GRID_BASICA, JsonObj, httpOptions)
      .pipe(map((itens) => {
        console.log('Dados a serem salvos', itens);
        return itens;
      })).toPromise().then((res) => {
        console.log('Data salva com sucesso', res);
        return res;
      });
  }

  public editarDados(ItemParaEditar: EditarModel, Id: number) {
    const post: EditarModel = {
      descricao: ItemParaEditar.descricao,
      grupo_Nome: ItemParaEditar.grupo_Nome,
      grupo_GrupoInterno: ItemParaEditar.grupo_GrupoInterno,
      grupo_Email: ItemParaEditar.grupo_Email,
      grupo_Origem: ItemParaEditar.grupo_Origem,
      grupo_DataCadastro: ItemParaEditar.grupo_DataCadastro,
      imagem: ItemParaEditar.imagem,
      politicasenha_Politicasenha: ItemParaEditar.politicasenha_Politicasenha,
      politicasenha_Alteracaosenha: ItemParaEditar.politicasenha_Alteracaosenha,
      select: ItemParaEditar.select,
      status_Status: ItemParaEditar.status_Status,
      status_Motivo: ItemParaEditar.status_Motivo,
      status_Data: ItemParaEditar.status_Data,
      status_Expiracao: ItemParaEditar.status_Expiracao
    };
    const JsonObj = JSON.stringify(post);
    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json'
        })
    };

    console.log('mas 1', Id);
    console.log('mas 2', JsonObj);
    console.log('mas 3', ItemParaEditar);

    return this.httpClient
      .put(Config.URLS.BASE + Config.URLS.URL_DADOS_DEV[0].DADOS_GRID_BASICA + '/' + ItemParaEditar.id, JsonObj, httpOptions)
      .pipe(map((itens) => {
        console.log('Dados a serem editados', itens);
        return itens;
      })).toPromise().then((res) => {
        console.log('Data editados com sucesso', res);
        return res;
      });
  }

  public deletarDados(ItemParaDeletar) {
    return this.httpClient
      .delete(Config.URLS.BASE + Config.URLS.URL_DADOS_DEV[0].DADOS_GRID_BASICA + '/' + ItemParaDeletar)
      .pipe(map((itens) => {
        console.log('Dados a serem deletados', itens);
        return itens;
      })).toPromise().then((res) => {
        console.log('Data deletados com sucesso', res);
        return res;
      });
  }
  private tratarAcao(data: any) {
    switch (data.idSubject) {
      case Config.OBJETOS_GRID.GRIDMODELO:
        this.fechDataBasicGrid(data.escopoPesquisa)
          .subscribe();
        break;
      case Config.OBJETOS_GRID.GRID_FILTRO_DESCRICAO:
        this.filtrarGridDescricao(data.escopoPesquisa,data.dados.inputPesquisa)
          .subscribe();
        break;

    }
  }
  public empty(): EditarModel {
    return <EditarModel>{
      descricao: '',
      grupo_Nome: '',
      grupo_GrupoInterno: '',
      grupo_Email: '',
      grupo_Origem: '',
      grupo_DataCadastro: '',
      imagem: '',
      politicasenha_Politicasenha: '',
      politicasenha_Alteracaosenha: '',
      select: '',
      status_Status: '',
      status_Motivo: '',
      status_Data: '',
      status_Expiracao: ''
    };
  }
}

import { Config } from './../environments/config';
import { Injectable } from '@angular/core';

@Injectable()
export class GridService {


  private _currentRow: any;
  private _gridContext: any[] = [];
  constructor() { }

  public set currentRow(gridRow: any[]) {
    // context: string, varName: string, value: any
    this._currentRow = gridRow;
  }

  public get currentRow(): any[] {
    return this._currentRow;
  }


  public get GridContext() {
    return this._gridContext;
  }


  public setGridContext(context: string, varName: string, value: any) {
    const obj = {
      context: context,
      name: varName,
      value: value
    };
    this._gridContext.push(obj);
  }

  public getGridContext(context: string, varName: string) {
    let res: any = {
      context: null,
      name: null,
      value: []
    };
    this._gridContext.forEach(element => {
      if (element.context === context && element.name === varName) {
        res = element;
      }
    });
    return res;
  }
  public clearGridContext() {
    console.log("chegou clear");
    this._gridContext = [{
      context: null,
      name: null,
      value: []
    }];
  }
}

import { Injectable } from '@angular/core';
import { EditarModel } from '../models/editar.model';
import { GridService } from './grid.service';

@Injectable()
export class EditarService {
  private _displayModal = false;
  private _displayRota = false;
  private _titulo: string;
  private _objetosTela: EditarModel = {
    descricao: '',
    grupo_Nome: '',
    grupo_GrupoInterno: '',
    grupo_Email: '',
    grupo_Origem: '',
    grupo_DataCadastro: '',
    imagem: '',
    politicasenha_Politicasenha: '',
    politicasenha_Alteracaosenha: '',
    select: '',
    status_Status: '',
    status_Motivo: '',
    status_Data: '',
    status_Expiracao: '',
  };
  constructor(public gridSrv: GridService) {

  }
  set titulo(title: string) {
    this._titulo = title;
  }
  get titulo() {
    return this._titulo;
  }

  set displayModal(show: boolean) {
    this._displayModal = show;
  }
  set displayRota(show: boolean) {
    this._displayRota = show;
  }

  get displayModal() {
    return this._displayModal;
  }
  get displayRota() {
    return this._displayRota;
  }

  set objetosTela(data: EditarModel) {

    this._objetosTela.descricao = data.descricao;
    this._objetosTela.grupo_Nome = data.grupo_Nome;
    this._objetosTela.grupo_GrupoInterno = data.grupo_GrupoInterno;
    this._objetosTela.grupo_Email = data.grupo_Email;
    this._objetosTela.grupo_Origem = data.grupo_Origem;
    this._objetosTela.grupo_DataCadastro = data.grupo_DataCadastro;
    this._objetosTela.imagem = data.imagem;
    this._objetosTela.id = data.id;
    this._objetosTela.politicasenha_Politicasenha = data.politicasenha_Politicasenha;
    this._objetosTela.politicasenha_Alteracaosenha = data.politicasenha_Alteracaosenha;
    this._objetosTela.select = data.select;
    this._objetosTela.status_Status = data.status_Status;
    this._objetosTela.status_Motivo = data.status_Motivo;
    this._objetosTela.status_Data = data.status_Data;
    this._objetosTela.status_Expiracao = data.status_Expiracao;
  }
  get objetosTela(): EditarModel {
    return this._objetosTela;
  }
}

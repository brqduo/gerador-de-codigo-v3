import { Config } from './../environments/config';
import { ComandBarModel } from './../models/comandBar.model';
import { Injectable } from '@angular/core';

@Injectable()
export class ComandBarService {
  private _ComandBarBtn: ComandBarModel[];
  constructor() { }
  get ComandBarBtn() {
    return this._ComandBarBtn = [
      {
        icone: 'fa fa-plus',
        nome: 'Novo',
        destino: Config.IDSUBJECT.rotaIncluir
      },
      {
        icone: 'fa fa-pencil-square-o',
        nome: 'Editar',
        destino: Config.IDSUBJECT.rotaEditar
      },
      {
        icone: 'fa fa-cog',
        nome: 'Permissão',
        destino: Config.IDSUBJECT.modalIncluir
      },
      {
        icone: 'fa fa-file',
        nome: 'Log',
        destino: Config.IDSUBJECT.modalEditar
      },
      {
        icone: 'fa fa-times',
        nome: 'Deletar',
        destino: Config.IDSUBJECT.deletar
      }
    ];
  }
}

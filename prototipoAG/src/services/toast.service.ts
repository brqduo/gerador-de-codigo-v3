import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ToastModel } from './../models/toast.model';
@Injectable()
export class ToastService {

  constructor(private msgSrv: MessageService) {
  }

  public success(msgOBj: ToastModel) {
    this.msgSrv.add(
      {
        severity: 'success',
        summary: msgOBj.title,
        detail: msgOBj.subtitle
      }
    );
  }
  public error(msgOBj: ToastModel) {
    this.msgSrv.add(
      {
        severity: 'error',
        summary: msgOBj.title,
        detail: msgOBj.subtitle
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { EditarComponent } from '../editar/editar.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers:[EditarComponent]
})
export class HomeComponent implements OnInit {

  constructor(public editarComp: EditarComponent) { }

  ngOnInit() {
    this.editarComp.ngOnInit();
  }

}

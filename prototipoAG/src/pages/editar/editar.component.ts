import { GridService } from './../../services/grid.service';
import { Config } from './../../environments/config';
import { ToastModel } from './../../models/toast.model';
import { ToastService } from './../../services/toast.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from '../../services/util.service';
import { DataService } from '../../services/dataservice.service';
import { EditarService } from '../../services/editar.service';
@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent implements OnInit {
  validarErro: any;
  tipoProcesso: string;
  display: boolean;
  iD_Item: number;
  constructor(
    public utilSrv: UtilService
    , public dataSrv: DataService
    , public route: Router
    , public toastSrv: ToastService
    , public editarSrv: EditarService
  ) {

  }
  ngOnInit() {
    this.utilSrv.specificSubjects.modalIncluir
      .subscribe((data) => {
        this._montarTela_Modal();
      });
    this.utilSrv.specificSubjects.rotaIncluir
      .subscribe((data) => {
        this._montarTela_Rota();
      });
    this.utilSrv.specificSubjects.rotaEditar
      .subscribe((data) => {
        console.log('rotaEditar', data);
        this.editarSrv.titulo = 'Editar';
        this._montarTela_Rota(data);
      });
    this.utilSrv.specificSubjects.modalEditar
      .subscribe((data) => {
        console.log('modalEditar', data);
        this.editarSrv.titulo = 'Editar';
        this._montarTela_Modal(data);
      });
  }

  private _montarTela_Modal(data?) {
    if (data === undefined) {
      this.tipoProcesso = Config.TIPO_PROCESSO.salvar;
      this.editarSrv.displayModal = true;
      this.editarSrv.displayRota = false;
      this.editarSrv.objetosTela = this.dataSrv.empty();
      this.editarSrv.titulo = 'Incluir';
      this.editarSrv.displayModal = true;
      this.editarSrv.displayRota = false;
    } else {
      this.editarSrv.displayModal = true;
      this.tipoProcesso = Config.TIPO_PROCESSO.editar;
      this.editarSrv.displayModal = true;
      this.editarSrv.displayRota = false;
      this.editarSrv.objetosTela = data.dados;
      this.iD_Item = data.dados.id;
      this.editarSrv.titulo = 'Editar';
      this.editarSrv.displayModal = true;
      this.editarSrv.displayRota = false;
    }
  }
  private _montarTela_Rota(data?) {
    if (data === undefined) {
      this.tipoProcesso = Config.TIPO_PROCESSO.salvar;
      this.editarSrv.displayRota = true;
      this.editarSrv.displayModal = false;
      this.editarSrv.objetosTela = this.dataSrv.empty();
      this.editarSrv.titulo = 'Incluir';
      this.editarSrv.displayModal = false;
      this.editarSrv.displayRota = true;
      this.route.navigateByUrl('Editar');
    } else {
      console.log('dados vindo da grid na linha 69', data);
      this.tipoProcesso = Config.TIPO_PROCESSO.editar;
      this.editarSrv.displayRota = true;
      this.editarSrv.displayModal = false;
      this.editarSrv.objetosTela = data.dados;
      this.iD_Item = data.dados.id;
      this.editarSrv.titulo = 'Editar';
      this.editarSrv.displayModal = false;
      this.editarSrv.displayRota = true;
      this.route.navigateByUrl('Editar');
    }
  }
  salvar() {
    console.log('DENTRO DE SALVAR DO EDITARCOMPONENT', this.editarSrv.objetosTela);
    if (this.tipoProcesso === Config.TIPO_PROCESSO.salvar) {
      this.dataSrv.salvarDados(this.editarSrv.objetosTela)
        .then((result) => {
          const msgObj: ToastModel = {
            title: 'Item foi salvo',
            subtitle: 'O item foi salvo com sucesso'
          };
          this.toastSrv.success(msgObj);
        }).catch((err) => {
          const msgObj: ToastModel = {
            title: 'Item foi Salvo',
            subtitle: 'O item foi salvo com suceso'
          };
          this.toastSrv.error(msgObj);
        });
      this.route.navigateByUrl('Home');
    } else {
      console.log('DENTRO DE SALVAR NA PARTE DE EDIÇÃO', this.iD_Item);
      this.dataSrv.editarDados(this.editarSrv.objetosTela, this.iD_Item)
        .then((result) => {
          const msgObj: ToastModel = {
            title: 'Item foi salvo',
            subtitle: 'O item foi salvo com sucesso'
          };
          this.toastSrv.success(msgObj);
          this.route.navigateByUrl('Home');
        }).catch((err) => {
          const msgObj: ToastModel = {
            title: 'Item foi editado',
            subtitle: 'O item foi editado com suceso'
          };
          this.toastSrv.error(msgObj);
          this.route.navigateByUrl('Home');
        });
    }
  }
  voltar() {
    const msgObj: ToastModel = {
      title: 'Cancelado',
      subtitle: 'A operacao foi cancelada a pedido do usuario'
    };
    this.toastSrv.error(msgObj);
    this.editarSrv.displayModal = false;
    this.editarSrv.displayRota = false;
    this.route.navigateByUrl('Home');
  }

}

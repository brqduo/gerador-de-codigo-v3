import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ComponentesModule } from '../components/components.module';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

// Pages

import { HomeComponent } from '../pages/home/home.component';
import { EditarComponent } from '../pages/editar/editar.component';

// Serviços

import { DataService } from '../services/dataservice.service';
import { UtilService } from '../services/util.service';
import { EditarService } from '../services/editar.service';
import { ToastService } from 'src/services/toast.service';
import { ComandBarService } from 'src/services/comandBar.service';
import { GridService } from './../services/grid.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { PrimeNGModule } from '../primeNG.module';

import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  { path: 'Home', component: HomeComponent },
  { path: 'Editar', component: EditarComponent },
  { path: '', redirectTo: 'Home', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EditarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ComponentesModule,
    PrimeNGModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    DataService,
    UtilService,
    EditarService,
    ComandBarService,
    ToastService,
    MessageService,
    ConfirmationService,
    GridService
  ],
  entryComponents: [
    EditarComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

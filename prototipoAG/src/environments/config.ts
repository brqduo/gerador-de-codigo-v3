export const Config = {
  URLS: {
    BASE: 'http://localhost:5500',
    URL_DADOS_DEV: [{ 
      DADOS_GRID_BASICA: '/pessoasAngular',
      DADOS_GRID_FILTRO_DESCRICAO: '/pessoasAngular?descricao_like=',
      DADOS_GRID_FILTRO_GRUPONOME: '/pessoasAngular?grupo_Nome_like=',
    }]
  },
  OBJETOS_GRID: {
    GRIDMODELO: 'GRIDMODELO',
    GRID_FILTRO_DESCRICAO: 'GRID_FILTRO_DESCRICAO',
  },
  GRIDCONTEXT: {
    LinhaSelecionadas: 'LinhaSelecionadas',
    UltimaLinhaSelecionada: 'UltimaLinhaSelecionada'
  },
  OBJETOS_SUBJECT: {
    filtro_comandbar: 'FILTROCOMAND'
  },
  TIPO_PROCESSO: {
    editar: 'editar',
    salvar: 'salvar'
  },
  TIPOMENSAGEM: {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
  },
  CRUD_BTN_IDs: {
    incluir: '10',
    editar: '20',
    excluir: '30',
    listar: '40',
    exportar: '50',
  },
  CAMINHODADOS: {
    url_dsv: '../assets/data/',
    url_hmg: '../assets/data/',
    url_prd: '../assets/data/'
  },
  CAMINHO_COLUMNS_DEF: {
    url_dsv: '../assets/data/',
    url_hmg: '../assets/data/',
    url_prd: '../assets/data/'
  },
  AMBIENTES: {
    dsv: 'dsv',
    hmg: 'hmg',
    prd: 'prd'
  },
  ARQUIVOSJSON: {
    pesquisa: 'pesquisa.json',
    botoes: 'botoes.json',
    gridColunas: 'gridColumns.json',
    gridDados: 'gridData.json',
    columns: 'columns.json',
    campostemplate: 'campostemplate.json'
  },
  IDSUBJECT: {
    pesquisa: 'pesquisa',
    grid: 'grid',
    comandBar: 'comandBar',
    deletar: 'deletar',
    modalEditar: 'modalEditar',
    modalIncluir: 'modalIncluir',
    rotaEditar: 'rotaEditar',
    rotaIncluir: 'rotaIncluir',
    textofiltro: 'textofiltro',
    dadoslidos: 'dadoslisos',
    gridfiltro: 'gridfiltro'
  },
  PESQUISA_AVANCADA: {
    maxColumns: 4,
    filtro_ordenacao: [
      { label: 'Ascendente', value: 0 },
      { label: 'Descendente', value: 1 }
    ]
  }
};

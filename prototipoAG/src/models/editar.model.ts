export interface EditarModel {
  id?: Number;
  descricao: string;
  grupo_Nome: string;
  grupo_GrupoInterno: string;
  grupo_Email: string;
  grupo_Origem: string;
  grupo_DataCadastro: string;
  imagem: string;
  politicasenha_Politicasenha: string;
  politicasenha_Alteracaosenha: string;
  select: string;
  status_Status: string;
  status_Motivo: string;
  status_Data: string;
  status_Expiracao: string;
}

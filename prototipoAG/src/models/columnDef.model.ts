export interface ColumnDefModel {
  headerName: string;
  field: string;
  width: number;
  autoHeight: boolean;
  cellRenderer: string;
  cellRendererParams: object;
  cellStyle: object;
  headerClass: string;
  cellClass: string;
}

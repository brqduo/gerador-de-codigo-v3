export interface DataSubjectModel {
    dados: any;
    idComponent:string;
    idSubject: string;
    escopoPesquisa: string;
}

export interface ToastModel {
  title: string;
  subtitle: string;
}

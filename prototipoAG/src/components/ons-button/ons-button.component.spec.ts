import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsButtonComponent } from './ons-button.component';

describe('OnsButtonComponent', () => {
  let component: OnsButtonComponent;
  let fixture: ComponentFixture<OnsButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

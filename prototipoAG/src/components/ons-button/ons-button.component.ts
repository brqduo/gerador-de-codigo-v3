import { DataService } from './../../services/dataservice.service';

import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from '../../services/util.service';
import { ComandBarService } from 'src/services/comandBar.service';
import { ComandBarModel } from '../../models/comandBar.model';
import { Config } from './../../environments/config';
import { ToastService } from 'src/services/toast.service';
import { GridService } from 'src/services/grid.service';

@Component({
  selector: 'app-ons-button',
  templateUrl: './ons-button.component.html',
  styleUrls: ['./ons-button.component.scss']
})
export class OnsButtonComponent implements OnInit {
  filtro: string;
  comandBar: any[];
  @Input() PesquisaId;
  constructor(
    public comandBarSrv: ComandBarService,
    public utilSrv: UtilService,
    private toastSrv: ToastService,
    public dataSrv: DataService,
    public gridSrv: GridService,
    public route: Router
  ) { }

  ngOnInit() {
    this.comandBar = this.comandBarSrv.ComandBarBtn;
  }
  filter(event) {
    console.log('evento', event);
    this.utilSrv.ChamarFuncao(event, Config.IDSUBJECT.grid, Config.IDSUBJECT.textofiltro, this.PesquisaId);
  }
  goto(event: ComandBarModel) {
    switch (event.destino) {
      case Config.IDSUBJECT.rotaEditar: {
        console.log('Estou sendo executado', this.gridSrv.getGridContext(this.PesquisaId, Config.GRIDCONTEXT.LinhaSelecionadas).value);
        if (this.gridSrv.getGridContext(this.PesquisaId, Config.GRIDCONTEXT.LinhaSelecionadas).value.length === 0) {
          const error = {
            title: 'Error',
            subtitle: 'Nenhum item foi selecionado'
          };
          this.toastSrv.error(error);
        } else {
          if (this.gridSrv.getGridContext(this.PesquisaId, Config.GRIDCONTEXT.LinhaSelecionadas).value.length > 1) {
            const error = {
              title: 'Error',
              subtitle: 'Items selecionados em demasia'
            };
            this.toastSrv.error(error);
          } else {
            const gridL = this.gridSrv.getGridContext(this.PesquisaId, Config.GRIDCONTEXT.LinhaSelecionadas);
            if (gridL.value.length === 1) {
              console.log('PESQUISA-ONS-BUTTON', gridL);
              this.utilSrv.ChamarFuncao(gridL.value[0], Config.IDSUBJECT.comandBar, Config.IDSUBJECT.rotaEditar, this.PesquisaId);
            }
          }
        }
      }
        break;
      case Config.IDSUBJECT.rotaIncluir: {
        this.utilSrv.ChamarFuncao(' ', Config.IDSUBJECT.comandBar, Config.IDSUBJECT.rotaIncluir, this.PesquisaId);
        this.route.navigateByUrl('Editar');
      }
        break;
      case Config.IDSUBJECT.modalEditar: {

        if (this.gridSrv.getGridContext(this.PesquisaId, Config.GRIDCONTEXT.LinhaSelecionadas).value.length === 0) {
          const error = {
            title: 'Error',
            subtitle: 'Nenhum item foi selecionado'
          };
          this.toastSrv.error(error);
        } else {
          if (this.gridSrv.getGridContext(this.PesquisaId, Config.GRIDCONTEXT.LinhaSelecionadas).value.length > 1) {
            const error = {
              title: 'Error',
              subtitle: 'Items selecionados em demasia'
            };
            this.toastSrv.error(error);
          } else {
            const gridL = this.gridSrv.getGridContext(this.PesquisaId, Config.GRIDCONTEXT.LinhaSelecionadas);
            if (gridL.value.length === 1) {
              console.log('PESQUISA-ONS-BUTTON', gridL);
              this.utilSrv.ChamarFuncao(gridL.value[0], Config.IDSUBJECT.comandBar, Config.IDSUBJECT.modalEditar, this.PesquisaId);
            }
          }
        }
      }
        break;
      case Config.IDSUBJECT.deletar: {
        if (this.utilSrv.gridSelectedRows.length === 0) {
          console.log(this.utilSrv.gridSelectedRows);
          const error = {
            title: 'Error',
            subtitle: 'Nenhum item selecionado'
          };
          this.toastSrv.error(error);
        } else {
          if (this.utilSrv.gridSelectedRows.length > 1) {
            // Error. Items selecionados em demasia
            const error = {
              title: 'Error',
              subtitle: 'Items selecionados em demasia'
            };
            this.toastSrv.error(error);
          } else {
            if (this.utilSrv.gridSelectedRows.length === 1) {
              this.dataSrv.deletarDados(this.utilSrv.gridSelectedRows[0].id);
            }
          }
        }
      }
        break;
    }
  }

}

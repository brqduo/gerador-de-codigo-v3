import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';

import { OnsButtonComponent } from './ons-button/ons-button.component';
import { OnsPesquisaComponent } from './ons-pesquisa/ons-pesquisa.component';
import { OnsGridComponent } from './ons-grid/ons-grid.component';
import { PrimeNGModule } from '../primeNG.module';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  imports: [CommonModule,
    PanelModule,
    InputTextModule,
    ButtonModule,
    PrimeNGModule,
    FormsModule,
    AgGridModule.withComponents([OnsGridComponent])
  ],
  exports: [
    CommonModule,
    BrowserAnimationsModule,
    InputTextModule,
    OnsButtonComponent,
    OnsPesquisaComponent,
    OnsGridComponent
  ],
  declarations: [OnsButtonComponent,
    OnsPesquisaComponent,
    OnsGridComponent]
})
export class ComponentesModule { }

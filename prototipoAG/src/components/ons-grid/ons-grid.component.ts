import { GridService } from './../../services/grid.service';
import { Config } from './../../environments/config';
import { Component, OnInit, Input } from '@angular/core';
import { Grid, GridOptions } from 'ag-grid-community';
import { DataService } from '../../services/dataservice.service';
import { UtilService } from '../../services/util.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ons-grid',
  templateUrl: './ons-grid.component.html',
  styleUrls: ['./ons-grid.component.scss']
})
export class OnsGridComponent implements OnInit {
  private _gridOptions: GridOptions;
  private _selectedRows: any;
  private _currentRow: any;
  private _gridApi: any;
  @Input() PesquisaId;

  public get CurrentRow() {
    return this._currentRow;
  }

  public get selectedRows(): any {
    return this._selectedRows;
  }

  public get gridOptions(): GridOptions {
    return this._gridOptions;
  }

  public get gridApi(): any {
    return this._gridApi;
  }


  constructor(
    public route: Router
    , public utilSrv: UtilService
    , public dataSrv: DataService
    , public gridSrv: GridService
  ) {
    this._gridOptions = {
      rowSelection: 'multiple',
      rowMultiSelectWithClick: true
    };
  }
  ngOnInit() {
    this._gridOptions = {
      columnDefs: this.createColumnDefs(),
      context: this.PesquisaId,
      //   rowData: this.createRowData(),
      rowSelection: 'multiple',
      groupSelectsChildren: true,
      rowMultiSelectWithClick: true,
      rowHeight: 100,
      onRowDoubleClicked: (event) => {
        this.utilSrv.ChamarFuncao(event.data, Config.IDSUBJECT.grid, Config.IDSUBJECT.rotaEditar, this.PesquisaId);
        this.route.navigateByUrl('Editar');
      },
      onRowSelected: (event) => {
        this._currentRow = event.data;
        this.gridSrv.setGridContext(event.context, Config.GRIDCONTEXT.UltimaLinhaSelecionada, event.data);
        console.log('ULTIMA LINHA SELECIONADA. EVENTO VINDO DA LINHA 70 DA PAGINA DE GRID.TS', event);
        this.getRows(event);
      },
      suppressCellSelection: true,
      /* suppressRowClickSelection: true, */
      onGridReady: (params) => {
        params.api.getFilterInstance(``);
        params.api.sizeColumnsToFit();
        this._gridApi = params.api;
        this.gridSrv.clearGridContext();
        this.utilSrv.ChamarFuncao('', 'grid', Config.OBJETOS_GRID.GRIDMODELO, this.PesquisaId);
        window.addEventListener('resize', function () {
          setTimeout(function () {
            params.api.sizeColumnsToFit();
          });
        });
      },

    };
    this.utilSrv.FuncaoChamada
      .subscribe(data => {
        this.tratarSubjects(data);

      });
  }

  tratarSubjects(data) {
    // console.log(data);
    if (data.escopoPesquisa === this.PesquisaId) {
      switch (data.idSubject) {
        case Config.IDSUBJECT.textofiltro:
          console.log('FILTRANDO TEXTO', data.dados);

          this._gridApi.setQuickFilter(data.dados);
          break;
        case Config.IDSUBJECT.dadoslidos:
          this.gridApi.setRowData(this.dataSrv.dados);
          break;
        case Config.IDSUBJECT.gridfiltro:
          this.gridApi.setRowData(this.dataSrv.dados);
          break;
      }
    }
  }

  getRows(event: any) {

    this.gridSrv.setGridContext(event.context,Config.GRIDCONTEXT.UltimaLinhaSelecionada,event.data);
    this.gridSrv.setGridContext(event.context,Config.GRIDCONTEXT.LinhaSelecionadas,this.gridOptions.api.getSelectedRows());
  }

  // specify the columns
  private createColumnDefs() {
    return [
      {
        headerName: '', field: 'select', width: 30, checkboxSelection: true, aggFunc: 'sum',
        cellStyle: { 'border-left': 'none !important', 'border-right': 'none !important' }
      },
      {
        headerName: 'Id',
        field: 'id',
        width: 0,
        hide: true,
        autoHeight: true
      },
      {
        headerName: '',
        field: 'imagem',
        width: 100,
        autoHeight: true,
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
          innerRenderer: function (params) {
            const flag =
              '<img border="0" width="100" height="80" src="https://i.imgur.com/HWtHrk3.png">';
            return '<span style="cursor: default;">' + flag + ' ' + params.value + '</span>';
          }
        },
        cellStyle: { 'border-left': 'none !important', 'left': '0px !important' },
        headerClass: 'grid-image-header',
        cellClass: 'grid-imgage-cell'
      },
      {
        headerName: 'Grupo',
        field: 'grupo',
        //  cellStyle: { 'white-space': 'normal' },
        getQuickFilterText: function (param) {
          // tslint:disable-next-line: max-line-length
          return param.data.grupo_Nome + ';' + param.data.grupo_GrupoInterno + ';' + param.data.grupo_Email + ';' + param.data.grupo_Origem + ';' + param.data.grupo_DataCadastro;
        },
        cellRenderer: function (param) {
          // console.log('dados deparam', param);

          return '<span class="span-grid-label">Nome: </span><span>' + param.data.grupo_Nome + '</span><br>' +
            '<span class="span-grid-label">grupo interno: </span><span>' + param.data.grupo_GrupoInterno + '</span><br>' +
            '<span class="span-grid-label">e-mail: </span><span>' + param.data.grupo_Email + '</span><br>' +
            '<span class="span-grid-label">Origem: </span><span>' + param.data.grupo_Origem + '</span>' +
            '<span class="span-grid-label">Cadastrado em: </span><span>' + param.data.grupo_DataCadastro + '</span>';
        }
      },
      {
        headerName: 'Status',
        field: 'status',
        getQuickFilterText: function (param) {
          // tslint:disable-next-line: max-line-length
          return param.data.status_Status + ';' + param.data.status_Motivo + ';' + param.data.status_Data + ';' + param.data.status_Expiracao;
        },
        cellRenderer: function (param) {
          return '<span class="span-grid-label">status: </span><span>' + param.data.status_Status + '</span><br>' +
            '<span class="span-grid-label">motivo: </span><span>' + param.data.status_Motivo + '</span><br>' +
            '<span class="span-grid-label">data: </span><span>' + param.data.status_Data + '</span><br>' +
            '<span class="span-grid-label">expiração: </span><span>' + param.data.status_Expiracao + '</span>';
        }
      },
      {
        headerName: 'Política Senha',
        field: 'politicasenha',
        getQuickFilterText: function (param) {
          // tslint:disable-next-line: max-line-length
          return param.data.politicasenha_Politicasenha + ';' + param.data.politicasenha_Alteracaosenha;
        },
        cellRenderer: function (param) {
          return '<span class="span-grid-label">política de senha: </span><span>' + param.data.politicasenha_Politicasenha + '</span><br>' +
            '<span class="span-grid-label">alteração senha: </span><span>' + param.data.politicasenha_Alteracaosenha + '</span>';
        }
      },
      {
        headerName: 'Descrição Comentário',
        field: 'descricao'
      },

    ];
  }

  // specify the data
  private createRowData() {
    this.dataSrv.fechDataBasicGrid(this.PesquisaId)
      .subscribe(data => {
        console.log('data restored');
      });
  }

  callRendererImagem() {

  }

}

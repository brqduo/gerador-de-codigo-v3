import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsGridComponent } from './ons-grid.component';

describe('OnsGridComponent', () => {
  let component: OnsGridComponent;
  let fixture: ComponentFixture<OnsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

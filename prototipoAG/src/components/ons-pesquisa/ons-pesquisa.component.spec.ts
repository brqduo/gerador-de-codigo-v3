import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnsPesquisaComponent } from './ons-pesquisa.component';

describe('OnsPesquisaComponent', () => {
  let component: OnsPesquisaComponent;
  let fixture: ComponentFixture<OnsPesquisaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnsPesquisaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnsPesquisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { trigger, transition, animate, style, state } from '@angular/animations';
import { DataService } from 'src/services/dataservice.service';
import { UtilService } from 'src/services/util.service';
import { Config } from '../../environments/config';


@Component({
  selector: 'app-ons-pesquisa',
  templateUrl: './ons-pesquisa.component.html',
  styleUrls: ['./ons-pesquisa.component.scss'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('simpleFadeAnimation', [

      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [
        style(
          { opacity: 0 }
        ),
        animate(1000)
      ]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave', [
        style(
          { opacity: 0 }
        ),
        animate(1000)
      ])
    ]
    )]
})
export class OnsPesquisaComponent implements OnInit {
  buscaAvancada: any = true;
  inputPesquisa: any;
  inputPesquisaAvancada: any;
  animateStringAvancada = 'hideAdvancedSearch';
  animateStringSimples = 'showAdvancedSearch';
  btnPesquisaLabel = 'Pesquisa Avançada';
  @Input() ComponentId;
  @Input() ComponentTitulo;
  @Input() PesquisaId;
  Component: any;

  constructor(private dataSrv: DataService,
    private utilSrv: UtilService) {

  }

  ngOnInit() {
  }

  pesquisar(target) {
    if (this.inputPesquisa !== undefined && this.inputPesquisa.trim() !== '') {
      const valores = {
        inputPesquisa: this.inputPesquisa.trim(),
        inputPesquisaAvancada: ((this.inputPesquisaAvancada === undefined) ? '' : this.inputPesquisaAvancada)
      };
      console.log(valores);
      this.utilSrv.ChamarFuncao(valores, this.ComponentId, Config.OBJETOS_GRID.GRID_FILTRO_DESCRICAO, this.PesquisaId);
      //this.utilSrv.ChamarFuncao(valores, this.ComponentId, Config.IDSUBJECT.pesquisa, this.PesquisaId);
    } else {
      this.utilSrv.alerta('Campo pesquisa não pode ser vazio.', 'Pesquisa Vazia', Config.TIPOMENSAGEM.error);
    }
  }

  pesquisaAvancada() {
    console.log('o q eu recebo?', this.buscaAvancada);

    if (this.buscaAvancada === true) {
      this.btnPesquisaLabel = 'Pesquisa Simples';
      this.animateStringSimples = 'hideAdvancedSearch';
      this.animateStringAvancada = 'showAdvancedSearch';
      this.buscaAvancada = false;
    } else {
      this.animateStringAvancada = 'hideAdvancedSearch';
      this.animateStringSimples = 'showAdvancedSearch';
      this.btnPesquisaLabel = 'Pesquisa Avançada';
      this.buscaAvancada = true;
    }

    /*    setTimeout(() => {
         this.buscaAvancada = !this.buscaAvancada;
         this.animateString = 'showAdvancedSearch';
       }, 300); */
  }

  onEnterKeyDown(event) {
    if (event.target.value !== '' && event.target.value !== undefined) {
       //console.log(event.target.value);
    }
  }

  onKeyDown(event) {
    if (event.target.value !== '' && event.target.value !== undefined) {
      // console.log(event.target.value);
    }
    // (keyup)="onKeyDown($event)"
  }
}
